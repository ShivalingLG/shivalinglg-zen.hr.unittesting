﻿using VSTest = Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using NUnit.Mocks;
using System.Collections.Generic;

namespace Zen.Hr.Logic.Tests
{
    //[TestFixture]
    [VSTest.TestClass]
    public class UserServiceTests
    {
        private UserService concern;
        private DynamicMock userRepoMock;
        private List<User> users;
        private List<User> activeUsers;

        [VSTest.TestInitialize()]
        public void TestInit()
        {
            users = new List<User>();
            users.AddRange(new User[]{
            new User { Name = "Wolverine", IsActive = true },
            new User { Name = "Magneto", IsActive = true },
            new User { Name = "Lockheed", IsActive = false }, 
            new User { Name = "Hawkeye", IsActive = false },
            new User { Name = "Captainn America", IsActive = true }
            });
            activeUsers = users.FindAll(user => user.IsActive);
            userRepoMock = new DynamicMock(typeof(IUserDataAccess));
        }

        [VSTest.TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            userRepoMock.ExpectAndReturn("GetAllUsers", users.ToArray());
            userRepoMock.ExpectAndReturn("GetAllActiveUsers", activeUsers.ToArray());
            concern = new UserService((IUserDataAccess)userRepoMock.MockInstance);
            // unit test goes here
            VSTest.Assert.AreEqual(concern.GetUsers(false).Length, 5, "Invalid number of users");
        }

        [VSTest.TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            userRepoMock.ExpectAndReturn("GetAllUsers", users.ToArray());
            userRepoMock.ExpectAndReturn("GetAllActiveUsers", activeUsers.ToArray());
            concern = new UserService((IUserDataAccess)userRepoMock.MockInstance);
            // unit test goes here
            VSTest.Assert.AreEqual(concern.GetUsers(true).Length, 3, "Invalid number of active users");
        }
    }
}
