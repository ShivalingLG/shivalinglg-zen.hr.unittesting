﻿namespace Zen.Hr.Logic
{
    public class User
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}